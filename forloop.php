
<?php
// if we want to print a single or group of statements multiple times we have to use for loop

for($a=1;$a<=10;$a++){
    echo " the value of a is:" .$a."<br>";
}

$num= 5;
for($i=0;$i<=10;$i++){
    echo $num. " X ".$i." = ". $num*$i." <br>"; 
}
?>